import React from 'react'
import classes from './ItemCard.module.css'
const ItemCard = (props) => {
    return (
        <div className={classes['card-content']}>
          <div className={classes['card-symbol']}>
          T
          </div>
        <textarea
          name="titleList"
          id="titleList"
          cols="15"
          rows="1"
          value={props.content}
        ></textarea>
        <div className={classes["add-item"]}>
          <i class="bx bxs-edit-alt"></i>
        </div>
      </div>
    )
}

export default ItemCard

import React from "react";
import { Route, Switch } from "react-router-dom";
import Customers from "../pages/Customers";
import Dashboard from "../pages/Dashboard";
import Places from "../pages/Places";
import Settings from "../pages/Settings";
function Router() {
  return (
    <Switch>
      <Route path="/" exact component={Dashboard} />
      <Route path="/customers" component={Customers} />
      <Route path="/places" component={Places} />
      <Route path="/settings" component={Settings} />
    </Switch>
  );
}

export default Router;

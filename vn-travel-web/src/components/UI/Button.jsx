import React from 'react'
import classes from './Button.module.css'
function Button() {
    return (
        <div className={classes['form-field']}>
            <input type="text" className={classes['form-input']}/>
            <label className={classes['form-label']}>Name</label>
        </div>
    )
}

export default Button

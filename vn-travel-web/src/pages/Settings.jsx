import React from "react";
import ItemCard from "../components/setting-page/item-card/ItemCard";
import Button from "../components/UI/Button";
import classes from "./Settings.module.css";
const Settings = () => {
  const list = ["Province", "Tag", "Category"];
  const itemOfList = ["An Nhon Tay", "An Phu", "Cu Chi"];
  return (
    <div className={classes.container}>
      {list.map((unit) => (
        <div className={classes["unit-contain"]}>
          <div className={classes["contain-header"]}>
            <textarea
              name="titleList"
              id="titleList"
              cols="15"
              rows="1"
              value={unit}
            ></textarea>
            <div className={classes["add-item"]}>
              <a>
                {" "}
                <i class="bx bx-plus"></i>
              </a>
            </div>
          </div>
          {itemOfList.map((item) => (
            <ItemCard content={item} />
          ))}
        </div>
      ))}
      <div>
     
      </div>
    </div>
  );
};

export default Settings;

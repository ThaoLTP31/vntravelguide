const Plan = require("../models/Plan");
const User = require("../models/User");
const mongoose = require("mongoose").set('useFindAndModify', false);
const express = require("express");
const router = express.Router();
const requireAuth = require("../middleware/requireAuth");
const requireRole = require("../middleware/requireRole");

//@route GET v1/plans
//@desc get plan by userId
//@access private
//@role user
router.get('/', requireAuth, async (req, res, next) => {
    try {

        const planner = req.body.userAuth;
        let plans = []
        plans = await Plan.find({ user: planner.id })
        if (plans) {
            return res.status(200).json({
                success: true,
                message: "Get plan successfully",
                plans: plans
            })
        }
        res.status(500).json({
            success: false,
            message: "Get plan unsuccessfully"
        })

    } catch (error) {
        console.log(err.message)
        res.status(500).json({
            success: false,
            message: "Internal error server",
        });
    }
})

//@route POST v1/plans
//@desc create new plan
//@access private
//@role user
router.post("/", requireAuth, async (req, res, next) => {

    try {
        const planner = req.body.userAuth;
        let plan = new Plan({
            name: req.body.name,
            start: req.body.start,
            end: req.body.end,
            note: req.body.note,
            photoUrl: (req.body.photoUrl.startsWith("https://") ? req.body.photoUrl : `https://${req.body.photoUrl}`) || "",
            user: planner.id
        })

        plan = await plan.save()
        if (plan) {
            return res.json({
                success: true,
                message: "Plan created successfully",
                plan: plan
            })
        }
        else {
            return res.status(500).json({
                success: false,
                message: "Plan created unsuccessfully",
            });
        }

    } catch (error) {
        console.log(error.message)
        res.status(500).json({
            success: false,
            message: "Internal error server",
        });
    }
})


//@route PUT v1/plans
//@desc update plan when user delete
//@access private
//@role user
router.put('/delete', requireAuth, async (req, res, next) => {



});
module.exports = router

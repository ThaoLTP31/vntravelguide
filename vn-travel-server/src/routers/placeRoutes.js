const Place = require("../models/Place");
const express = require("express");
const router = express.Router();
const requireAuth = require("../middleware/requireAuth");
const requireRole = require("../middleware/requireRole");

//GET ALL:PLACES
router.get("/", async (req, res) => {
  try {
    const placeList = await Place.find();
    return res.status(200).json({
      success: true,
      message: "Get places successfully",
      places: placeList,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      success: false,
      message: "Internal Server Error",
    });
  }
});

//POST: Create new place
router.post("/", requireAuth, async (req, res, next) =>
  requireRole("admin", req, res, next, async (req, res, next) => {

    let place = new Place({
      name: req.body.name,
      description: req.body.description,
      longtitude: req.body.longtitude,
      lattitude: req.body.lattitude,
      tags: req.body.tags,
      rate: req.body.rate,
      weight: req.body.weight,
      province: req.body.province,
      status: req.body.status,
      closeTime: req.body.closeTime,
      openTime: req.body.openTime,
      price: req.body.price,
    });

    try {
      place = await place.save();
      if (!place) {
        return res.status(500).json({
          success: false,
          message: "Place created unsuccessfully",
        });
      }
      return res.status(200).json({
        success: true,
        message: "Place created successfully",
        place: place,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        message: "Internal server error",
      });
    }
  })
);

module.exports = router;

const express = require("express");
const requireAuth = require("../middleware/requireAuth");
const requireRole = require("../middleware/requireRole");
const router = express.Router();
const { Tag } = require("../models/Tag");
const { route } = require("./authRoutes");
//@route GET v1/tags
//@desc Get all tags
//@access public
//@role all
router.get("/", async (req, res) => {
  try {
    const tagList = await Tag.find();
    return res.status(200).json({
      message: "Get all tag successfully",
      suceessful: true,
      tags: tagList,
    });
  } catch (err) {
    return res.status(500).json({
      message: err.message,
      suceessful: false,
    });
  }
});

//@route POST v1/tags
//@desc Create new tag
//@access private
//@role admin
router.post("/", requireAuth, async (req, res, next) =>
  requireRole("admin", req, res, next, async (req, res, next) => {
    try {
      let tag = new Tag({
        name: req.body.name,
      });
      tag = await tag.save();
      return res.status(200).json({
        message: "Create tag successfully ",
        suceessful: true,
        tag: tag,
      });
    } catch (error) {
      return res.status(500).json({
        message: error.message,
        suceessful: false,
      });
    }
  })
);

//@route PUT v1/tag/:id
//@desc Update tag info
//@access private
//@role admin
router.put("/:tagId", requireAuth, async (req, res, next) =>
  requireRole("admin", red, res, next, async (req, res, next) => {
      
  })
);

module.exports = router;

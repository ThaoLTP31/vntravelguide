const mongoose = require("mongoose");
const dateVietNam = require('../utils/Timezone')
const planSchema = new mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    start: {
        type: Date,
        default: Date.now
    },
    end: {
        type: Date,
        default: Date.now
    },
    note: {
        type: String
    },
    createdAt: {
        type: Date,
        default: dateVietNam
    },
    updatedAt: {
        type: Date,
        default: dateVietNam
    },
    photoUrl: {
        type: String
    },
    isHidden: {
        type: Boolean,
        default: false
    },
    user: {
        type: mongoose.Types.ObjectId,
        ref: "users"
    }
})

planSchema.method("toJSON", function () {
    const { __v, ...object } = this.toObject();
    const { _id: id, ...result } = object;
    return { ...result, id };
});

module.exports = mongoose.model('plans', planSchema)
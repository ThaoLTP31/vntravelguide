const mongoose = require("mongoose");

const reviewSchema = mongoose.Schema({
    title: {
        type: String,
        require: true
    },
    content: {
        type: String,
        require: true
    },
    rate: {
        type: mongoose.Schema.Types.Decimal128,
        default: 0
    },
    likeCount: {
        type: mongoose.Schema.Types.Decimal128
    },
    likedUser: {
        // type:  mongoose.Types.Array<mongoose.Schema.Types.ObjectId>
        // ref: 'Place'
    },
    createAt: {
        type: Date,
        default: Date.now
    },
    visitedTime: {
        type: Date,
        default: Date.now
    },

    updatedAt: {
        type: Date,
        default: Date.now
    },
    place: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'places'
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    },
    isHidden: {
        type: Boolean,
        default: false
    }
})

reviewSchema.method("toJSON", function () {
    const { __v, ...object } = this.toObject();
    const { _id: id, ...result } = object;
    return { ...result, id };
});
module.exports = mongoose.model('reviews', reviewSchema)


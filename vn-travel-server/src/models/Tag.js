const mongoose = require('mongoose')

const tagSchema = mongoose.Schema({
    name: {
        type: String,
        require: true,
        default: ''
    },
    isHidden: {
        type: Boolean,
        default: false
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    },
})

tagSchema.method("toJSON", function () {
    const { __v, ...object } = this.toObject();
    const { _id: id, ...result } = object;
    return { ...result, id };
});
module.exports = mongoose.model("tags", tagSchema);
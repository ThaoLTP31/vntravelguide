const mongoose = require("mongoose");
const { STATUS } = require("./enum");

const placeSchema = mongoose.Schema({
    name: {
        type: String,
        require: true,
    },
    description: {
        type: String,
        default: "",
    },
    longtitude: {
        type: mongoose.Schema.Types.Decimal128,
        require: true,
    },
    lattitude: {
        type: mongoose.Schema.Types.Decimal128,
        require: true,
    },
    tags: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'tags'
    }],
    rate: {
        type: mongoose.Schema.Types.Decimal128,
        require: true,
        default: 0
    },
    reviewCount: {
        type: Number,
        default: 0
    },
    weight: {
        type: Number
    },
    province: {
        type: String,
        require: true
    },
    status: {
        type: String,
        default: STATUS.PUBLIC,
    },
    closeTime: mongoose.Schema.Types.Decimal128,
    openTime: mongoose.Schema.Types.Decimal128,
    price: {
        start: {
            type: Number,
            default: 0
        },
        end: {
            type: Number,
            default: 0
        }

    },
    isHidden: {
        type: Boolean,
        default: false
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    },
});

placeSchema.method("toJSON", function () {
    const { __v, ...object } = this.toObject();
    const { _id: id, ...result } = object;
    return { ...result, id };
});
module.exports = mongoose.model("places", placeSchema);

const mongoose = require('mongoose')
const { IMAGECATEGORY } = require('./enum')

const imageSchema = mongoose.Schema({
    url: {
        type: String,
        require: true
    },
    place: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'places',
        default: null
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    },
    category: {
        type: String,
        default: IMAGECATEGORY.PLACE
    },
    review: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'reviews'
    },
    isHidden: {
        type: Boolean,
        default: false
    }

})

imageSchema.method("toJSON", function () {
    const { __v, ...object } = this.toObject();
    const { _id: id, ...result } = object;
    return { ...result, id };
});

module.exports = mongoose.model("images", imageSchema);
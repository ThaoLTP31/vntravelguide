const mongoose = require('mongoose')

const sectionSchema = mongoose.Schema({
    plan: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'plans'
    },
    start: {
        type: Date,
        default: Date.now
    },
    end: {
        type: Date,
        default: Date.now
    },
    places: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'places',
        default: []
    }],
    createdAt: {
        type: Date,
        default: Date.now
    },
    updateddAt: {
        type: Date,
        default: Date.now
    },
    note: {
        type: String
    },
    isHidden: {
        type: Boolean,
        default: false
    }

})

sectionSchema.method("toJSON", function () {
    const { __v, ...object } = this.toObject();
    const { _id: id, ...result } = object;
    return { ...result, id };
});

module.exports = mongoose.model('sections', sectionSchema)
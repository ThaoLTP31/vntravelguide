const mongoose = require('mongoose')

const provinceSchema = mongoose.Schema.Types({
    name: {
        type: String,
        require: true
    },
    color: {
        type: String,
        default: '#fff'
    },
    isHidden: {
        type: Boolean,
        default: false
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    },
})

provinceSchema.method("toJSON", function () {
    const { __v, ...object } = this.toObject();
    const { _id: id, ...result } = object;
    return { ...result, id };
});
module.exports = mongoose.model("provinces", provinceSchema);
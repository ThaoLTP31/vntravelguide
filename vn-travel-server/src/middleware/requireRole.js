const User = require("../models/User");
const express = require("express");

const requireRole = async (routeRole, req, res, next, cd) => {
  try {
    const snapshot = await User.findOne({ email: req.body.userAuth.email });
    if (snapshot) {
      if (
        (routeRole == 'admin' && !snapshot.isUser) ||
        (routeRole == 'user' && snapshot.isUser)
      ) {
        if (routeRole == 'user') {
          console.log(req.params.userId)
          console.log(snapshot.id)
          if (req.params.userId == snapshot.id) {
            cd(req, res, next);
          }
          else {
            return res.status(500).json({
              message: "Permission denied",
              successfull: false
            });
          }
        }
        else {
          cd(req, res, next);
        }
      } else {
        return res.status(500).json({
          message: "Permission denied",
          successfull: false
        });
      }
    } else {
      return res.status(404).json({
        message: "User not found",
        successfull: false
      });
    }
  } catch (error) {

    console.log(error)
    return res.status(403).json({
      message: error.message,
      successfull: false
    });
  }
};
module.exports = requireRole;
